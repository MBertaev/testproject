package com.murad.testproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity {

    private String[] OptRozn = {"Опт.", "Розн."};
    private String[] Stoleshnica = {"Прямая", "Г-образная", "П-образная"};
    private String[] Material = {"Серия А", "Серия В"};
    private String[] Stenovaya = {"Прямая", "Г-образная", "П-образная"};
    private String[] Moyka = {"Вырез", "Монтаж"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testproject);

        ArrayAdapter<String> OptRoznAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, OptRozn);
        OptRoznAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) findViewById(R.id.spOptRozn);
        spinner.setAdapter(OptRoznAdapter);

        ArrayAdapter<String> StoleshnicaAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Stoleshnica);
        StoleshnicaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner1 = (Spinner) findViewById(R.id.spStoleshnica);
        spinner1.setAdapter(StoleshnicaAdapter);

        ArrayAdapter<String> MaterialAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Material);
        MaterialAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner2 = (Spinner) findViewById(R.id.spMaterial);
        spinner2.setAdapter(MaterialAdapter);

        ArrayAdapter<String> StenovayaAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Stenovaya);
        StenovayaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner3 = (Spinner) findViewById(R.id.spStenovaya);
        spinner3.setAdapter(StenovayaAdapter);

        ArrayAdapter<String> MoykaAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Moyka);
        MoykaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner4 = (Spinner) findViewById(R.id.spMoyka);
        spinner4.setAdapter(MoykaAdapter);



    }
}